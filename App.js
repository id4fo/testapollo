import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Auth from "@aws-amplify/auth";
import {
  ApolloClient,
  ApolloProvider,
  gql,
  InMemoryCache,
} from "@apollo/client";

import {
  GENRES_QUERY,
  EVENT_QUERY,
  EVENT_QUERY2,
  PROFILE_QUERY,
} from "./src/queries/queries";

export default function App() {
  const GRAPHQL_SERVER_URL = "https://test-app-server.boombazoo.com/graphql";

  let authToken = "";
  // login automatically
  Auth.signIn("iresh", "dudejairesh")
    .then((user) => {
      console.log(
        "login done! Token: \n",
        user.signInUserSession.accessToken.jwtToken
      );
      authToken = user.signInUserSession.accessToken.jwtToken;
    })
    .catch((err) => {
      if (!err.message) {
        console.log("Error when signing in: ", err);
        Alert.alert(I18n.get("ErrorSignIn"));
      } else {
        console.log("Error when signing in: ", err.message);
        Alert.alert(I18n.get("ErrorSignIn"));
      }
    });

  //client
  const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: GRAPHQL_SERVER_URL,
    headers: {
      authorization: `Bearer ${authToken}`,
      localeid: "de",
    },
    connectToDevTools: true,
  });

  const SAMPLE_QUERY = EVENT_QUERY2;

  client
    .query({
      query: gql`
        ${SAMPLE_QUERY}
      `,
    })
    .then((result) => {
      console.log(`App.js | Query: \n ${SAMPLE_QUERY}`);
      console.log(`-----------------------------------`);
      console.log(`App.js | Query Result:  ${JSON.stringify(result, null, 4)}`);
    })
    .catch((err) => {
      console.log(`App.js | Query: \n ${SAMPLE_QUERY}`);
      console.log(`-----------------------------------`);
      console.log(`App.js | Error is:\n ${JSON.stringify(err, null, 4)}`);
    });

  return (
    <ApolloProvider client={client}>
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <StatusBar style="auto" />
      </View>
    </ApolloProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
