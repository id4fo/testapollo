const axios = require('axios');
const gql = require('graphql-tag');
const graphql = require('graphql');
const {print} = graphql;

const createUser = gql`
  mutation createUser(
    $username: String
    $firstname: String
    $lastname: String
    $email: String
    $roles: [String]
    $phonenumber: String
  ) {
    createUser(
      username: $username
      firstname: $firstname
      lastname: $lastname
      email: $email
      roles: $roles
      phonenumber: $phonenumber
    ) {
      id
      username
      firstname
      lastname
      roles
      email
      active
    }
  }
`;

const API_URL_Local = 'https://test-app-server.boombazoo.com:8443/graphql';
const API_URL_AWS = 'http://172.31.33.115:4001/graphql';

const isRunningLocally = !process.env.AWS_EXECUTION_ENV;
console.log('IsLocal: ', isRunningLocally);

exports.handler = async (event, context, callback) => {
  try {
    const graphqlData = await axios({
      url: isRunningLocally ? API_URL_Local : API_URL_AWS,
      method: 'post',
      headers: {
        locale:
          'locale' in event.request.userAttributes
            ? event.request.userAttributes.locale
            : 'de',
      },
      data: {
        query: print(createUser),
        variables: {
          username: event.userName,
          firstname:
            'given_name' in event.request.userAttributes
              ? event.request.userAttributes.given_name
              : '',
          lastname:
            'family_name' in event.request.userAttributes
              ? event.request.userAttributes.family_name
              : '',
          email:
            'email' in event.request.userAttributes
              ? event.request.userAttributes.email
              : '',
          roles: ['REGULAR'],
          phonenumber:
            'phone_number' in event.request.userAttributes
              ? event.request.userAttributes.phone_number
              : '',
        },
      },
    });
    const response = {
      graphqlData: JSON.stringify(graphqlData.data),
    };
    console.log(response);
    // Return to Amazon Cognito
    callback(null, event);
  } catch (err) {
    console.log('error creating user: ', err);
    callback(null, event);
  }
};
