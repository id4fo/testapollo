export const GENRES_QUERY = `
  query AllGenres {
    allGenres {
      id
      name
    }
  }
`;

export const EVENT_QUERY = `
  query {
    events(genres: [], skip: 0, take: 10) {
      events {
        id
        name
        startDate
        endDate
        isSubscribed
      }
      take
      skip
      totalCount
    }
  }
`;

export const EVENT_QUERY2 = `
  query {
    events(genres: ["German_rap"], skip: 0, take: 10) {
      events {
        id
        name
        startDate
        endDate
        isSubscribed
      }
      take
      skip
      totalCount
    }
  }
`;

export const PROFILE_QUERY = `
  query {
    me {
      id
      username
      firstname
      lastname
      email
      phonenumber
      gender
      genderVisible
      birthDate
      active
      roles
      groups {
        id
      }
    }
  }
`;
