## TestApollo v3.0

This repo is created to test

## Pre-requisites:

- Set up your machine for [react-native](https://reactnative.dev/docs/environment-setup) and install all the required tools mentioned under _React Native CLI_
- [AWS Account](https://aws.amazon.com/amplify/)
- [Amplify CLI](https://docs.amplify.aws/)

## Configuring the project

1. Clone this repo to your local machine.

```
git clone git@bitbucket.org:id4fo/testapollo.git
cd TestApollo
```

2. Connect your app with amplify backend.

```
amplify pull --appId d29ezgvtj71vpt --envName dev
```

## Running the application

1. Install client dependencies.

```
npm install
```

2. On macOS, install all the pod dependecies.

```
cd cd TestApollo/ios
pod install
cd ..
```

3. Launch the React Native app in your simulator under your project directory.

- For iOS

```
npx react-native run-ios
```

- For android

```
npx react-native run-android
```
